# Mahdi Arezoumandi

import sys
import math as Math

class Viterbi(object):
    def __init__(self, hmm):
        self.hmm = hmm
        self.states = hmm.states

    def _init_backpointer(self, output):
        back_pointer = [{}]
        path = {}

        for state in self.states:
            back_pointer[0][state] = self.hmm.start_prob(state) * \
                    self.hmm.output_prob(state, output[0])
            path[state] = [state]

        alpha = sum(back_pointer[0].values())
        for key in back_pointer[0]:
            back_pointer[0][key] = back_pointer[0][key] / alpha

        return back_pointer, path

    def most_likely_sequence(self, output):
        lookup_lambda = lambda x: x[0]
        hmm = self.hmm
        states = self.states
        back_pointer, path = self._init_backpointer(output)

        trans_prob = hmm.transitions_probabilities
        output_prob = hmm.output_probabilities

        for t in range(1, len(output)):
            back_pointer.append({})
            newpath = {}

            for state in states:
                current_max_prob = (0, None)

                for state_0 in states:
                    state_0_prob = back_pointer[t-1][state_0] * \
                            trans_prob[state_0][state]

                    if state_0_prob >= current_max_prob[0]:
                        current_max_prob = (state_0_prob, state_0)

                (prob, max_state) = current_max_prob
                newpath[state] = path[max_state] + [state]

                back_pointer[t][state] = prob * output_prob[state][output[t]]

            alpha = sum(back_pointer[t].values())
            for key in back_pointer[t]:
                back_pointer[t][key] = back_pointer[t][key] / alpha

            path = newpath

        n = t if len(output) != 1 else 0

        (prob, state) = max((back_pointer[n][state], state) for state in self.states)

        return (prob, path[state])
