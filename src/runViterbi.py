# Mahdi Arezoumandi

import sys
from viterbi import Viterbi


class RunViterbi(object):
    def __init__(self, data, hmm, inputOut, verbose):
        self.data = data
        self.hmm = hmm
        self.inputOut = inputOut
        self.run(verbose)

    def run(self, verbose):
        data = self.data
        hmm = self.hmm
        viterbi = Viterbi(hmm)

        print("Start probabilities:\n")
        for state in data.statekeys:
            print(state+' :\t' +("{0:.3f}".format(hmm.start_prob(state))))

        print("\nTransition probabilities:\n")

        for state in data.states:
            sys.stdout.write('\t\t' + state)
        sys.stdout.write('\n')
        sys.stdout.flush()
        for from_state in data.states:
            sys.stdout.write(from_state + ' :')
            for to_state in data.states:
                trans_prob = hmm.trans_prob(from_state, to_state)
                sys.stdout.write('\t' + "{0:.3f}".format(trans_prob))
            sys.stdout.write('\n')
            sys.stdout.flush()

        print("\nOutput probabilities:\n")

        print_outputs = (len(data.outputs) < 30) or verbose

        if not print_outputs:
            print("*" * 57)
            print("Too many outputs to display... calculating the outputs...")
            print("Run with '-v' to see all outputs")
            print("*" * 57)

        if print_outputs:
            for output in sorted(data.outputs):
                sys.stdout.write('\t\t' + output)
        if print_outputs:
            sys.stdout.write('\n')
            sys.stdout.flush()

        for state in data.states:
            sys.stdout.write(state + ' :')
            for output in sorted(data.outputs):
                out_prob = hmm.output_prob(state, output)
                if print_outputs:
                    sys.stdout.write('\t' + "{0:.3f}".format(float(out_prob)))
            if print_outputs:
                sys.stdout.write('\n')
                sys.stdout.flush()

        if self.inputOut != None:
            len_out_in = len(self.inputOut)

            _, mlsss = viterbi.most_likely_sequence(self.inputOut)
            print("\nYour Entered Outputs:\n")
            print(self.inputOut)
            print('')

            print(' output  |  calc input')

            for i in range(len_out_in):
                print('----------------------\n   ' + self.inputOut[i]+ '    |      '+ mlsss[i])

        overall_error = 0
        for i, sequence in enumerate(data.testing.sequences):
            print_mls = (i < 4) or verbose
            if (i == 4) and not verbose:
                print("")
                print("*" * 54)
                print("There are too many sequences to display... Calculating")
                print("Run with '-v' to see all outputs")
                print("*" * 54)

            outputs = sequence.outputs()
            inputs = sequence.inputs()
            _, mls = viterbi.most_likely_sequence(outputs)

            if print_mls:
                print("\nMost likely sequence #"+str(i)+":\n")
                print('input|  calc |output')

            errors = 0
            inputs_len = len(inputs)
            for i in range(inputs_len):
                if print_mls:
                    print('------------------\n'+inputs[i]+ '  |  '+ mls[i]+ '  |  '+ outputs[i])
                if inputs[i] != mls[i]:
                    errors += 1
                else:
                    pass

            err_percentage = errors / float(inputs_len)

            if print_mls:
                print('\nErrors: ' + str(errors) + ' / ' + str(len(inputs)) + ' = ' + str(err_percentage))

            seq_len = float(len(data.testing.sequences))
            overall_error += err_percentage / seq_len

        correct_percent = 1 - overall_error
        print("\nThe overall percent correct items is " +
                  "{0:.3f}".format(correct_percent) + "%")


