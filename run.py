# Mahdi Arezoumandi

import src
import getopt
import sys

from src.hmm import Hmm
from src.runViterbi import RunViterbi

def main():
    # Setup Variables
    data = None
    hmm = None
    inputOut = None
    verbose = False

    try:
        optlist, remainder = getopt.getopt(sys.argv[1:], 'p:i:vh')
        if len(optlist) == 0:
            print("***Options required***")
            usage()
    except getopt.GetoptError as err:
        print(str(err))
        usage()

    for option, argument in optlist:
        if option == "-v":
            verbose = True
            print("\nProvided Arguments: ")
            print(str(optlist) + "\n")

        elif option == "-h":
            usage()

        elif option == "-p":
            if argument not in ('1', '2'):
                print("You must input Data File number")
                usage()
            if argument == '1':
                data = src.Robot('data/robot_no_momemtum.data')
            elif argument == '2':
                data = src.Robot('data/robot_with_momemtum.data')

        elif option == '-i':
            inputOut = list(''+argument+'')
	hmm = Hmm(data)
    RunViterbi(data, hmm, inputOut, verbose)

def usage():
    print("""
Written By Mahdi Arezoumandi
	---
    Flags:
    ---
        -p  Data File (1,2) - 1: robot_no_momemtum.data - 2: robot_with_momemtum.data
        -v  verbose - Show all outputs
		-i  Sequence of first character of color names as output sequence
        -h  help
	---
	Usage:
    ---
        python run.py -p 1
		this run algorithm with robot_no_momemtum.data
		
		python run.py -p 1 -i ryrbgrgbrg
		this run algorithm with robot_no_momemtum.data and your output sequence
		
        Example Usage
        python run.py -p 2 -v
		this run algorithm with robot_with_momemtum.data and show all outputs
		
    """)

    sys.exit(2)

if __name__ == "__main__":
    main()

