Written By Mahdi Arezoumandi

---
    Flags:
    ---
        -p  Data File (1,2) - 1: robot_no_momemtum.data - 2: robot_with_momemtum.data
        -v  verbose - Show all outputs
		-i  Sequence of first character of color names as output sequence
        -h  help
	---
	Usage:
    ---
        python run.py -p 1
		this run algorithm with robot_no_momemtum.data
		
		python run.py -p 1 -i ryrbgrgbrg
		this run algorithm with robot_no_momemtum.data and your output sequence
		
        Example Usage
        python run.py -p 2 -v
		this run algorithm with robot_with_momemtum.data and show all outputs
		
	Also you can see sample outputs in ""Logs"" Directory.